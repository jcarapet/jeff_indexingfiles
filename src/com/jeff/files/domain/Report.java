package com.jeff.files.domain;

import java.util.ArrayList;
import java.util.List;

public class Report {
	List<MediaFile> mediaFiles = new ArrayList<MediaFile>();
	List<MediaFile> possibleMatches = new ArrayList<MediaFile>();
	List<MediaFile> probableMatches = new ArrayList<MediaFile>();

	public List<MediaFile> getMediaFiles() {
		return mediaFiles;
	}

	public List<MediaFile> getPossibleMatches() {
		return possibleMatches;
	}

	public List<MediaFile> getProbableMatches() {
		return probableMatches;
	}
}
