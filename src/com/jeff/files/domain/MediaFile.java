package com.jeff.files.domain;

public class MediaFile {
	private String path;
	private String name;
	private long size;
	private String fileType;
	private String saveDate;

	// - File types. JPEG, MP4, M2T RAW CR2
	// - Size
	// - Location
	// - Name
	// - Last save date
	// - Path
	// - Identify Raw Photo files and create subfolder for RAW files. Move them
	// to said subfolders.
	// - Identify potential duplicates
	// - Identify total hard drive space for each for each type of file
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getSaveDate() {
		return saveDate;
	}

	public void setSaveDate(String saveDate) {
		this.saveDate = saveDate;
	}

}
